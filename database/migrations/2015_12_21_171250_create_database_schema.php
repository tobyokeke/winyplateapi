<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateDatabaseSchema extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('users')){Schema::drop('users');}
        if(Schema::hasTable('relationships')){Schema::drop('relationships');}
        if(Schema::hasTable('likes')){Schema::drop('likes');}
        if(Schema::hasTable('tags')){Schema::drop('tags');}
        if(Schema::hasTable('recipies')){Schema::drop('recipies');}
        if(Schema::hasTable('features')){Schema::drop('features');}
        if(Schema::hasTable('posts')){Schema::drop('posts');}


        Schema::create('users', function (Blueprint $table) {

            $table->increments('id');

            $table->string('username');
            $table->string('password');
            $table->string('fullname');
            $table->string('email');
            $table->string('phone');
            $table->string('profilepic')->nullable();
            $table->string('bio',500)->nullable();

            $table->enum('gender',['Male','Female']);

            $table->timestamps();
        });

        Schema::create('relationships',function (Blueprint $table){
            $table->string('follower')->nullable();
            $table->string('following')->nullable();

            $table->timestamps();
        });

        Schema::create('posts',function (Blueprint $table){
            $table->increments('id');

            $table->string('photo');
            $table->string('caption',5000)->nullable();

            $table->integer('users_id',false,true);
            $table->foreign('users_id')->references('id')->on('users')->onUpdate('cascade');


            $table->timestamps();

        });

        Schema::create('recipies',function (Blueprint $table){
            $table->increments('id');

            $table->string('name');
            $table->string('content',5000);

            $table->integer('users_id',false,true);

            $table->foreign('users_id')->references('id')->on('users')->onUpdate('cascade');

            $table->timestamps();
        });

        Schema::create('likes',function (Blueprint $table){

            $table->integer('users_id',false,true);
            $table->integer('posts_id',false,true);

            $table->foreign('users_id')->references('id')->on('users')->onUpdate('cascade');
            $table->foreign('posts_id')->references('id')->on('posts')->onUpdate('cascade');

            $table->timestamps();
        });

        Schema::create('features',function (Blueprint $table){

            $table->integer('users_id',false,true);
            $table->integer('posts_id',false,true);

            $table->foreign('users_id')->references('id')->on('users')->onUpdate('cascade');
            $table->foreign('posts_id')->references('id')->on('posts')->onUpdate('cascade');

            $table->timestamps();
        });

        Schema::create('tags', function (Blueprint $table){
            $table->increments('id');

            $table->string('name');

            $table->integer('posts_id',false,true);
            $table->integer('users_id',false,true);

            $table->foreign('users_id')->references('id')->on('users')->onUpdate('cascade');
            $table->foreign('posts_id')->references('id')->on('posts')->onUpdate('cascade');

            $table->timestamps();

        });

        Schema::create('comments', function (Blueprint $table){
            $table->increments('id');
            $table->string('content',5000);
            $table->integer('users_id',false,true);
            $table->integer('posts_id',false,true);

            $table->foreign('users_id')->references('id')->on('users')->onUpdate('cascade');
            $table->foreign('posts_id')->references('id')->on('posts')->onUpdate('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //no rollback for initial schema
    }
}
