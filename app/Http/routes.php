<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

use Illuminate\Database\Eloquent\Model;

class User extends Model { }
class Tag extends Model { }
class Post extends Model { }
class Recipie extends Model { }
class Like extends Model { }
class Feature extends Model { }
class Relationship extends Model { }

$app->get('/', function () use ($app) {

    return "Welcome To WinyPlate API";
});


$app->get('/allusers', 'UserController@allUsers'); //done
$app->get('user/{id}', 'UserController@getUser'); //done
$app->get('post/{id}', 'UserController@getPost'); //done
$app->get('timeline/{uid}/{limit}/{offset}', 'UserController@getTimeline'); //done
$app->get('/tags','UserController@getTags'); //done
$app->get('/recipies','UserController@getRecipies'); //done
$app->get('/followers/{uid}','UserController@getFollowers'); //done
$app->get('/following/{uid}','UserController@getFollowing');


$app->post('/user/create','UserController@createUser'); //done
$app->post('/post/create','UserController@createPost'); //doone
$app->post('tag/create','UserController@createTag'); //done
$app->post('recipie/create','UserController@createRecipie'); //done

$app->post('login','UserController@login');
$app->post('feature/create','UserController@createFeature'); //done
$app->post('follow/create','UserController@createFollower'); //done
