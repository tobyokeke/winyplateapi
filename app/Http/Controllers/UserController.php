<?php
/**
 * Created by PhpStorm.
 * User: Toby
 * Date: 12/24/2015
 * Time: 3:31 AM
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller;

class UserController extends Controller {


    public function login(Request $request){
        $username = $request->input('u');
        $password = $request->input('p');
        $user =  \User::hydrateRaw("select * from users where username = '$username' and password = '$password'");


        return $username;
    }
    public function allUsers(){
        return \User::all();

    }
    public  function getTags(  ){
        return \Tag::all();
    }

    public  function getRecipies(  ){
        return \Tag::all();
    }

    public function getPost($id){
        $post = \Post::find($id);
        $post->photo =  'http://localhost:8000/WinyPlateApi' . $post->photo;
        return $post;

    }
    public function getUser($id){
        return \User::find($id);
    }

    public function getTimeline($uid,$limit,$offset){
        $timeline = \Post::hydrateRaw("select * from Posts limit $limit offset $offset");
        return $timeline;
    }

    public function getFollowers($uid){
        $followers = \Relationship::hydrateRaw("select * from relationships where following = '$uid'");
        return $followers;
    }

    public function getFollowing($uid){
        $followers = \Relationship::hydrateRaw("select * from relationships where follower = '$uid'");
        return $followers;
    }

    public function createTag(Request $request){
        $tag = new \Tag();
        $tag->name = $request->input('name');
        $tag->posts_id = $request->input('pid');
        $tag->users_id = $request->input('uid');
        $tag->save();

        return 'All done';
    }

    public function createRecipie(Request $request){
        $recipie = new \Recipie();
        $recipie->name = $request->input('name');
        $recipie->content = $request->input('content');
        $recipie->users_id = $request->input('uid');
        $recipie->save();
        return 'All done';
    }

    public function createFeature(Request $request){
        $feature = new \Feature();
        $feature->posts_id = $request->input('pid');
        $feature->users_id = $request->input('uid');
        $feature->save();
        return 'All done';
    }

    public function createFollower(Request $request){
        $follower = new \Relationship();
        $follower->follower = $request->input('follower');
        $follower->following = $request->input('following');
        $follower->save();
        return  'All done';
    }

    public function createUser(Request $request){
        $user = new \User();
        $user->fullname =  $request->input('fullname');
        $user->email =  $request->input('email');
        $user->username =  $request->input('username');
        $user->password =  $request->input('password');
        $user->gender =  $request->input('gender');
        $user->phone =  $request->input('phone');
        $user->bio =  $request->input('bio');
        $user->save();

        return 'your name is '. $request->input('fullname').' and your email is '.$request->input('email').
            ' and your profile has been saved';
    }

    public function createPost(Request $request){
        $filename = $request->file('image')->getClientOriginalName();
        // if(file_exists($filename)){};

        $request->file('image')->move('../assets/userUploads',$filename);
        $post = new \Post();
        $post->users_id = $request->input('uid');
        $post->caption = $request->input('caption');
        $post->photo =  '/assets/userUploads/' . $filename;

        $post->save();
        return 'All done';

    }

}